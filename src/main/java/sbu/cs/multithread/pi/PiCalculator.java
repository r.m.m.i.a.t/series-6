package sbu.cs.multithread.pi;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PiCalculator {
    private int numDigits;
    private MathContext mc;
    private BigDecimal pi;

    public PiCalculator(int numDigits) {
        this.numDigits = numDigits;
        mc = new MathContext(numDigits);
        pi = new BigDecimal(0);
    }

    public BigDecimal compute() throws InterruptedException {

        CountDownLatch counter = new CountDownLatch(numDigits);

        ExecutorService service = Executors.newFixedThreadPool(10);


        for (int i = 0; i < numDigits; i++) {

            int finalK = i;

            Thread thread = new Thread(() ->
            {
                synchronized ("lock") {
                    BigDecimal piK = piFunction(finalK);
                    pi = pi.add(piK);
                }

                counter.countDown();
            });

            service.submit(thread);
        }

        counter.await();
        service.shutdown();

        return pi.round(mc);
    }


    private BigDecimal piFunction(int x) {
        int k8 = 8 * x;

        BigDecimal val1 = new BigDecimal(4);
        val1 = val1.divide(new BigDecimal(k8 + 1), mc);

        BigDecimal val2 = new BigDecimal(-2);
        val2 = val2.divide(new BigDecimal(k8 + 4), mc);

        BigDecimal val3 = new BigDecimal(-1);
        val3 = val3.divide(new BigDecimal(k8 + 5), mc);

        BigDecimal val4 = new BigDecimal(-1);
        val4 = val4.divide(new BigDecimal(k8 + 6), mc);

        BigDecimal val = val1;

        val = val.add(val2);
        val = val.add(val3);
        val = val.add(val4);

        BigDecimal multiplier = new BigDecimal(16);
        multiplier = multiplier.pow(x);

        BigDecimal one = new BigDecimal(1);
        multiplier = one.divide(multiplier, mc);

        val = val.multiply(multiplier);

        return val;
    }
}
