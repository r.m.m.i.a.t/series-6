package sbu.cs.multithread.pi;

public class PICalculator {

    /**
     * calculate pi and represent it as string with given floating point number (numbers after .)
     * check test cases for more info
     * check pi with 1000 digits after floating point at https://mathshistory.st-andrews.ac.uk/HistTopics/1000_places/
     *
     * @param floatingPoint number of digits after floating point
     * @return pi in string format
     */
    public String calculate(int floatingPoint) throws InterruptedException {

        PiCalculator Pi = new PiCalculator( floatingPoint + 2);

        StringBuilder s = new StringBuilder(Pi.compute().toString());

        return (s.deleteCharAt(s.length() - 1).toString());
    }

}
