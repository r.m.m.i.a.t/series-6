package sbu.cs.exception;

import java.util.List;

public class Reader {

    /**
     * declare 2 Exception class. 1 for UnrecognizedCommand and 1 for NotImplementedCommand
     * iterate on function inputs and check for commands and throw exception when needed.
     *
     * @param args
     */
    public void readTwitterCommands(List<String> args) throws ApException {
        for (String arg : args) {
            if(Util.getNotImplementedCommands().contains(arg))
            {
                throw new NotImplementedCommands();
            }
        }
        for (String arg : args) {
            if(!Util.getNotImplementedCommands().contains(arg) && !Util.getImplementedCommands().contains(arg))
            {
                throw  new UnrecognizedCommands() ;
            }
        }
    }


    /**
     * function inputs are String but odd positions must be integer parsable
     * a valid input is like -> "ap", "2", "beheshti", "3992", "20"
     * throw BadInput exception when the string is not parsable.
     *
     * @param args
     */
    public void read(String...args) throws InvalidInput {
        String[] arg = args;
        for(int i = 1 ; i < arg.length ; i += 2)
        {
            try
            {
                Integer.parseInt(arg[i]);
            }
            catch (Exception E)
            {
                throw new InvalidInput();
            }
        }
    }

}
